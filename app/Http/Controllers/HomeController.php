<?php

namespace App\Http\Controllers;

use http\Message\Body;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Response;
use Carbon\Carbon;
use ZipStream\Exception\OverflowException;
use Illuminate\Support\Facades\Http;
class HomeController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {

        return view('welcome');
    }

    public function card()
    {
        ini_set('max_execution_time', 900);
        $Auth_API_url = "https://accept.paymobsolutions.com/api/auth/tokens";
        $register_order_API_url = "https://accept.paymobsolutions.com/api/ecommerce/orders";
        $payment_keys_API_url = "https://accept.paymobsolutions.com/api/acceptance/payment_keys";
        $card_pay_API_url = "https://accept.paymobsolutions.com/api/acceptance/iframes";
        DB::beginTransaction();
        try{

            $auhtrequestdata = ['api_key' => env('API_KEY')];
            $authresponse = Http::post($Auth_API_url, $auhtrequestdata);
            if($authresponse->getStatusCode()==201 || $authresponse->getStatusCode()==200){
                $authtoken=$authresponse["token"]; //get auth token from authentication request
                ////////////////////////////////////////////////////// register order
                ///{
                //  "auth_token":{{from previus step}}",
                //  "delivery_needed": "false",
                //  "merchant_id": "49046",
                //  "amount_cents": "100",
                //  "currency": "EGP",
                //  "items": []
                //}
                ///
                ///
                $registerOrderData = ['auth_token' => $authtoken,'delivery_needed' => "false",'merchant_id' =>  env('MERCHANT_ID'),
                                        'amount_cents' =>"100",'currency' =>"EGP",'items' =>[]
                                            ];
                $registerOrderresponse = Http::post($register_order_API_url, $registerOrderData);

                if($registerOrderresponse->getStatusCode()==201 || $registerOrderresponse->getStatusCode()==200){
                    $getorderId=$registerOrderresponse["id"]; //get order id from order request
                    ////////////////////////////////////////////////////// payment_keys
                    ///{{
                    //  "auth_token":{{from step one}}",
                    //  "amount_cents": "100",
                    //  "expiration": 3600,
                    //  "order_id": {{from previus step}},
                    //  "currency": "EGP",
                    //  "integration_id": {{ from dashboard }},
                    //  "lock_order_when_paid": "false"
                    //}
                    ///
                    $payment_keysData = ['auth_token' => $authtoken,'expiration' => "3600",'integration_id' =>  env('CARD_INTEGRATION'),
                        'amount_cents' =>"100",'currency' =>"EGP",'order_id' => $getorderId ,"lock_order_when_paid" => "false",
                        "billing_data"=> [
                                        "apartment" => "803",
                                        "email"=> "claudette09@exa.com",
                                        "floor"=> "42",
                                        "first_name"=> "Clifford",
                                        "street"=> "Ethan Land",
                                        "building"=> "8028",
                                        "phone_number"=> "+86(8)9135210487",
                                        "shipping_method"=> "PKG",
                                        "postal_code"=> "01898",
                                        "city"=> "Jaskolskiburgh",
                                        "country"=> "CR",
                                        "last_name"=> "Nicolas",
                                        "state"=> "Utah"
                                      ]
                                        ];

                    $payment_keysresponse = Http::post($payment_keys_API_url, $payment_keysData);

                    if($payment_keysresponse->getStatusCode()==201 || $payment_keysresponse->getStatusCode()==200){
                        $pay_token=$payment_keysresponse["token"]; //get pay token from pay  request
//
                        $final_url=$card_pay_API_url.'/'.env('IFRAME_ID').'?payment_token='.$pay_token;
                        return redirect()->away($final_url);
                        }
                        else{
                            return redirect()->back()->with('danger',"error while get payment token");
                            }

                    //                ///
                    //                ///
                    //                ///
                    //                /// ///////////////////////////////////////////////////////
                }else{
                    return redirect()->back()->with('danger', "Register Order Error");
                }
                ///
                ///
                ///
                ///
                /// ///////////////////////////////////////////////////////
            }else{
                return redirect()->back()->with('danger', "Cannot Authenticate Error");
            }


                 DB::commit();
                }catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->back()->with('danger', "Unexpected Error");
                } catch (\Throwable $th) {
                    DB::rollBack();
                    return redirect()->back()->with('danger', "some Thing  Error");
                }
        return view('welcome');
    }




    public function kiosh()
    {
        ini_set('max_execution_time', 900);
        $Auth_API_url = "https://accept.paymobsolutions.com/api/auth/tokens";
        $register_order_API_url = "https://accept.paymobsolutions.com/api/ecommerce/orders";
        $payment_keys_API_url = "https://accept.paymobsolutions.com/api/acceptance/payment_keys";
        $kiosh_pay_API_url = "https://accept.paymobsolutions.com/api/acceptance/payments/pay";
        DB::beginTransaction();
        try{

            $auhtrequestdata = ['api_key' => env('API_KEY')];
            $authresponse = Http::post($Auth_API_url, $auhtrequestdata);
            if($authresponse->getStatusCode()==201 || $authresponse->getStatusCode()==200){
                $authtoken=$authresponse["token"]; //get auth token from authentication request
                ////////////////////////////////////////////////////// register order
                ///{
                //  "auth_token":{{from previus step}}",
                //  "delivery_needed": "false",
                //  "merchant_id": "49046",
                //  "amount_cents": "100",
                //  "currency": "EGP",
                //  "items": []
                //}
                ///
                ///
                $registerOrderData = ['auth_token' => $authtoken,'delivery_needed' => "false",'merchant_id' =>  env('MERCHANT_ID'),
                    'amount_cents' =>"100",'currency' =>"EGP",'items' =>[]
                ];
                $registerOrderresponse = Http::post($register_order_API_url, $registerOrderData);

                if($registerOrderresponse->getStatusCode()==201 || $registerOrderresponse->getStatusCode()==200){
                    $getorderId=$registerOrderresponse["id"]; //get order id from order request
                    ////////////////////////////////////////////////////// payment_keys
                    ///{{
                    //  "auth_token":{{from step one}}",
                    //  "amount_cents": "100",
                    //  "expiration": 3600,
                    //  "order_id": {{from previus step}},
                    //  "currency": "EGP",
                    //  "integration_id": {{ from dashboard }},
                    //  "lock_order_when_paid": "false"
                    //}
                    ///
                    $payment_keysData = ['auth_token' => $authtoken,'expiration' => "3600",'integration_id' =>  env('KIOSK_INTEGRATION'),
                        'amount_cents' =>"100",'currency' =>"EGP",'order_id' => $getorderId ,"lock_order_when_paid" => "false",
                        "billing_data"=> [
                            "apartment" => "803",
                            "email"=> "claudette09@exa.com",
                            "floor"=> "42",
                            "first_name"=> "Clifford",
                            "street"=> "Ethan Land",
                            "building"=> "8028",
                            "phone_number"=> "+86(8)9135210487",
                            "shipping_method"=> "PKG",
                            "postal_code"=> "01898",
                            "city"=> "Jaskolskiburgh",
                            "country"=> "CR",
                            "last_name"=> "Nicolas",
                            "state"=> "Utah"
                        ]
                    ];

                    $payment_keysresponse = Http::post($payment_keys_API_url, $payment_keysData);

                    if($payment_keysresponse->getStatusCode()==201 || $payment_keysresponse->getStatusCode()==200){
                        $pay_token=$payment_keysresponse["token"]; //get pay token from pay  request

                        $kioshData = ['payment_token' => $pay_token,'source' =>[
                            'identifier' => "AGGREGATOR",
                            'subtype' => "AGGREGATOR",
                            ]
                        ];
                        $kiosk_response = Http::post($kiosh_pay_API_url, $kioshData);
                        if($kiosk_response->getStatusCode()==201 || $kiosk_response->getStatusCode()==200){
                                    dd('Pending '.$kiosk_response['pending'].' =================> success '.$kiosk_response['success'].' =================> bill_reference '.$kiosk_response['data']['bill_reference']);
                        }else{
                            return redirect()->back()->with('danger',"error while kiosh payment");
                        }
                    }
                    else{
                        return redirect()->back()->with('danger',"error while get payment token");
                    }

                    //                ///
                    //                ///
                    //                ///
                    //                /// ///////////////////////////////////////////////////////
                }else{
                    return redirect()->back()->with('danger', "Register Order Error");
                }
                ///
                ///
                ///
                ///
                /// ///////////////////////////////////////////////////////
            }else{
                return redirect()->back()->with('danger', "Cannot Authenticate Error");
            }


            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', "Unexpected Error");
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('danger', "some Thing  Error");
        }
        return view('welcome');
    }




    public function wallet()
    {
        ini_set('max_execution_time', 900);
        $Auth_API_url = "https://accept.paymobsolutions.com/api/auth/tokens";
        $register_order_API_url = "https://accept.paymobsolutions.com/api/ecommerce/orders";
        $payment_keys_API_url = "https://accept.paymobsolutions.com/api/acceptance/payment_keys";
        $wallet_pay_API_url = "https://accept.paymobsolutions.com/api/acceptance/payments/pay";
        DB::beginTransaction();
        try{

            $auhtrequestdata = ['api_key' => env('API_KEY')];
            $authresponse = Http::post($Auth_API_url, $auhtrequestdata);
            if($authresponse->getStatusCode()==201 || $authresponse->getStatusCode()==200){
                $authtoken=$authresponse["token"]; //get auth token from authentication request
                ////////////////////////////////////////////////////// register order
                ///{
                //  "auth_token":{{from previus step}}",
                //  "delivery_needed": "false",
                //  "merchant_id": "49046",
                //  "amount_cents": "100",
                //  "currency": "EGP",
                //  "items": []
                //}
                ///
                ///
                $registerOrderData = ['auth_token' => $authtoken,'delivery_needed' => "false",'merchant_id' =>  env('MERCHANT_ID'),
                    'amount_cents' =>"100",'currency' =>"EGP",'items' =>[]
                ];
                $registerOrderresponse = Http::post($register_order_API_url, $registerOrderData);

                if($registerOrderresponse->getStatusCode()==201 || $registerOrderresponse->getStatusCode()==200){
                    $getorderId=$registerOrderresponse["id"]; //get order id from order request
                    ////////////////////////////////////////////////////// payment_keys
                    ///{{
                    //  "auth_token":{{from step one}}",
                    //  "amount_cents": "100",
                    //  "expiration": 3600,
                    //  "order_id": {{from previus step}},
                    //  "currency": "EGP",
                    //  "integration_id": {{ from dashboard }},
                    //  "lock_order_when_paid": "false"
                    //}
                    ///
                    $payment_keysData = ['auth_token' => $authtoken,'expiration' => "3600",'integration_id' =>  env('WALLET_INTEGRATION'),
                        'amount_cents' =>"100",'currency' =>"EGP",'order_id' => $getorderId ,"lock_order_when_paid" => "false",
                        "billing_data"=> [
                            "apartment" => "803",
                            "email"=> "claudette09@exa.com",
                            "floor"=> "42",
                            "first_name"=> "Clifford",
                            "street"=> "Ethan Land",
                            "building"=> "8028",
                            "phone_number"=> "+86(8)9135210487",
                            "shipping_method"=> "PKG",
                            "postal_code"=> "01898",
                            "city"=> "Jaskolskiburgh",
                            "country"=> "CR",
                            "last_name"=> "Nicolas",
                            "state"=> "Utah"
                        ]
                    ];

                    $payment_keysresponse = Http::post($payment_keys_API_url, $payment_keysData);

                    if($payment_keysresponse->getStatusCode()==201 || $payment_keysresponse->getStatusCode()==200){
                        $pay_token=$payment_keysresponse["token"]; //get pay token from pay  request

                        $kioshData = ['payment_token' => $pay_token,'source' =>[
                            'identifier' => "01010101010",
                            'subtype' => "CASH",
                        ]
                        ];
                        $wallet_response = Http::post($wallet_pay_API_url, $kioshData);
                        if($wallet_response->getStatusCode()==201 || $wallet_response->getStatusCode()==200){
//                            dd('Pending '.$wallet_response['pending'].' =================> success '.$wallet_response['success'].' =================> redirect_url '.$wallet_response['redirect_url']);
                            $final_url=$wallet_response['iframe_redirection_url'];//iframe_redirection_url
                             return redirect()->away($final_url);
                        }else{
                            return redirect()->back()->with('danger',"error while wallet payment");
                        }
                    }
                    else{
                        return redirect()->back()->with('danger',"error while get payment token");
                    }

                    //                ///
                    //                ///
                    //                ///
                    //                /// ///////////////////////////////////////////////////////
                }else{
                    return redirect()->back()->with('danger', "Register Order Error");
                }
                ///
                ///
                ///
                ///
                /// ///////////////////////////////////////////////////////
            }else{
                return redirect()->back()->with('danger', "Cannot Authenticate Error");
            }


            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('danger', "Unexpected Error");
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with('danger', "some Thing  Error");
        }
        return view('welcome');
    }
}
